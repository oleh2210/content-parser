package com.chubuk.services;

import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.Word;

import java.util.Map;

public interface WordFunction {
    SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity);

}
