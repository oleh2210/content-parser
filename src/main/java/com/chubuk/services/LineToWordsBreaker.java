package com.chubuk.services;

import com.chubuk.SongInfo;
import com.chubuk.Word;

import java.util.HashMap;
import java.util.Map;

public class LineToWordsBreaker {

    public Map<String, Word> getWords(SongInfo songInfo) {
        Map<String, Word> stringWordMap = new HashMap<>();
        for (String line : songInfo.getLines()) {
            line = line.toLowerCase();
            for (String word : line.split("[!?:\\-.,\\s]+")) {
                if (word.equals("")) continue;
                if (stringWordMap.get(word) == null) {
                    stringWordMap.put(word, new Word(word));
                } else stringWordMap.get(word).count();
            }
        }
        return stringWordMap;
    }
}
