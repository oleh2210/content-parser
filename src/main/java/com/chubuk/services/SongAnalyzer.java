package com.chubuk.services;

import com.chubuk.SongInfo;
import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.Word;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SongAnalyzer {

    private List<WordFunction> wordFunctionList;
    private LineToWordsBreaker lineToWordsBreaker;


    public SongAnalyzer(List<WordFunction> wordFunctionList, LineToWordsBreaker lineToWordsBreaker) {
        this.wordFunctionList = wordFunctionList;
        this.lineToWordsBreaker = lineToWordsBreaker;
    }

    public List<SongStatisticEntity> analyzeSong(List<SongInfo> songInfo) {
        List<SongStatisticEntity> songStatisticEntities = new ArrayList<>();//будем хранить сонг статистик ентити

        for (SongInfo info : songInfo) {
            SongStatisticEntity songStatisticEntity = new SongStatisticEntity();
            Map<String, Word> wordMap = lineToWordsBreaker.getWords(info);
            for (WordFunction wordFunction : wordFunctionList) {
                wordFunction.apply(wordMap, songStatisticEntity);
            }
            songStatisticEntity.setDate(Calendar.getInstance().getTime());
            songStatisticEntity.setTimestamp(System.currentTimeMillis());
            songStatisticEntity.setContentUrl(info.getUrl());
            songStatisticEntity.setContent(String.join("\n", info.getLines()));
            songStatisticEntities.add(songStatisticEntity);
        }
        return songStatisticEntities;
    }
}