package com.chubuk.services;

import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.Word;
import com.chubuk.exception.TextAnalyzerServiceException;

import java.util.Map;

public class AmountPopularWord implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {

        if (songStatisticEntity == null) {
            throw new TextAnalyzerServiceException("SongStatisticEntity can't be null.");
        }
        if (stringWordMap == null) {
            throw new TextAnalyzerServiceException("Words map can't be null.");
        }
        Word word = stringWordMap.values()
                         .stream()
                         .max(Word::compareTo)
                         .get();
        songStatisticEntity.setMostPopularWords(word.getWord());
        return songStatisticEntity;
    }
}
