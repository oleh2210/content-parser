package com.chubuk.services;

import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.Word;
import com.chubuk.exception.TextAnalyzerServiceException;

import java.util.Map;

public class AmountUniqueWords implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {

        if (songStatisticEntity == null) {
            throw new TextAnalyzerServiceException("SongStatisticEntity can't be null.");
        }
        if (stringWordMap == null) {
            throw new TextAnalyzerServiceException("Words map can't be null.");
        }
        Integer number = (int) stringWordMap.values()
                           .stream()
                           .filter(word -> word.getAmount() == 1)
                           .count();
        songStatisticEntity.setAmountUniqueWords(number);
        return songStatisticEntity;
    }
}
