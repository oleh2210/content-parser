package com.chubuk.services;

import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.Word;
import com.chubuk.exception.TextAnalyzerServiceException;

import java.util.Map;

public class AmountAllWords implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap,
                                     SongStatisticEntity songStatisticEntity) {
        Integer number = stringWordMap.values()
                .stream()
                .mapToInt(Word::getAmount)
                .sum();
        songStatisticEntity.setTotalWordsAmount(number);
        return songStatisticEntity;
    }
}
