package com.chubuk.services;

import com.chubuk.enums.UserRole;

public class Role {
    private UserRole role;

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
