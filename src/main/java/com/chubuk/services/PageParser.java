package com.chubuk.services;

import com.chubuk.SongInfo;
import org.jsoup.nodes.Document;
import java.util.List;

public interface PageParser {

    List<SongInfo> parsWebPage(Document page);

    String getTargetUrl();
}
