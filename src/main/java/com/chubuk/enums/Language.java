package com.chubuk.enums;

public enum Language {
    ENGLISH, RUSSIAN, OTHER
}
