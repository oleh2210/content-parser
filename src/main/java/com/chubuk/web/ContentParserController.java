package com.chubuk.web;

import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.dao.SongStatisticDao;
import com.chubuk.services.TextAnalyzerService;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/statistics")
public class ContentParserController {

    private final SongStatisticDao songStatisticDao;
    private final TextAnalyzerService textAnalyzerService;

    public ContentParserController(SongStatisticDao songStatisticDao,
                                   TextAnalyzerService textAnalyzerService) {
        this.songStatisticDao = songStatisticDao;
        this.textAnalyzerService = textAnalyzerService;
    }
    @PutMapping
    public StatisticListDto calculateStatistics(@RequestBody CalculateStatisticsDto dto) {
        StatisticListDto statisticListDto = new StatisticListDto();
        List<SongStatisticEntity> songStatisticEntities = textAnalyzerService.startAnalyzer(dto.getUrls());
        statisticListDto.setUrlsCount(dto.getUrls().size());
        for (SongStatisticEntity songStatisticEntity : songStatisticEntities) {
            StatisticDto statisticDto = convertToDto(songStatisticEntity);
            statisticListDto.addStatisticDto(statisticDto);
        }
        return statisticListDto;
    }
    @GetMapping(value = "/{statistic_id}")
    public StatisticDto getStatistics(@PathVariable("statistic_id") Long id) {
        SongStatisticEntity byId = songStatisticDao.getById(id);
        StatisticDto statisticDto = convertToDto(byId);
        return statisticDto;
    }
    private StatisticDto convertToDto(SongStatisticEntity entity) {
        StatisticDto statisticDto = new StatisticDto();
        statisticDto.setId(entity.getId());
        statisticDto.setContentUrl(entity.getContentUrl());
        statisticDto.setLyrics(entity.getContent());
        statisticDto.setAmountSameWords(entity.getAmountSameWords());
        statisticDto.setTotalWordsAmount(entity.getTotalWordsAmount());
        statisticDto.setMostPopularWords(entity.getMostPopularWords());
        statisticDto.setAmountUniqueWords(entity.getAmountUniqueWords());
        statisticDto.setLanguage(entity.getLanguage());
        return statisticDto;
    }
}