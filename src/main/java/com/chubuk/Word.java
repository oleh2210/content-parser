package com.chubuk;

public class Word implements Comparable<Word> {

    private  String word;
    private  Integer amount = 0;

    public Word(String word) {
        this.word = word;
        amount++;
    }

    public void count(){
        amount++;
    }

    public String getWord() {
        return word;
    }

    public Integer getAmount() {
        return amount;
    }

    @Override
    public int compareTo(Word word) {
        return amount.compareTo(word.amount);
    }

}
