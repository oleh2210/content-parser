package com.chubuk.dao;

import com.chubuk.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<UserEntity, Long> {

    UserEntity findOneByEmail(String email);
}
