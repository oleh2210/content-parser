package com.chubuk.services;

import com.chubuk.exception.TextAnalyzerServiceException;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SongExtractorTest {

    private SongExtractor songExtractor;

    @Mock
    private Document document;

    @Mock
    private PageParser pageParser;

    @Before
    public void setUp() {
        List<PageParser> pageParsers = new ArrayList<>();
        pageParsers.add(pageParser);
        songExtractor = new SongExtractor(pageParsers);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void exceptionWillBeThrownWhenTheParserSheetIsEmpty() {
        SongExtractor songExtractor = new SongExtractor(new ArrayList<>());

        songExtractor.extractSong(document);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void thePageWillNotBeParsedWhenTheTargetIsNotTargetUrl() {
        Mockito.when(document.location()).thenReturn("Location");
        Mockito.when(pageParser.getTargetUrl()).thenReturn("GetUrl");

        songExtractor.extractSong(document);
    }

    @Test
    public void thePageWillBeParsedWhenTheLocationContainsTheTargetUrl() {
        Mockito.when(document.location()).thenReturn("Location");
        Mockito.when(pageParser.getTargetUrl()).thenReturn("Location");

        songExtractor.extractSong(document);

        Mockito.verify(pageParser).parsWebPage(document);
    }
}

