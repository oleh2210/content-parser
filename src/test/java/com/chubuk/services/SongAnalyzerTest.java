package com.chubuk.services;

import com.chubuk.SongInfo;
import com.chubuk.Word;
import com.chubuk.entities.SongStatisticEntity;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SongAnalyzerTest {

    @Mock
    private WordFunction wordFunction;

    @Mock
    private LineToWordsBreaker lineToWordsBreaker;

    @Test
    public void expectedEmptyListWillBeGetWhenTheSongInfoListIsEmpty() {
        List<SongInfo> songInfo = new ArrayList<>();
        SongAnalyzer songAnalyzer = new SongAnalyzer(new ArrayList<>(), lineToWordsBreaker);

        List<SongStatisticEntity> songStatisticEntities = songAnalyzer.analyzeSong(songInfo);

        Assertions.assertEquals(songStatisticEntities.size(), 0);
    }

    @Test
    public void songInfoParametersWillBeSetWhenInSongStatisticEntity() {
        final SongInfo songInfo = new SongInfo("name", singletonList("line"), "url");
        Map<String, Word> stringWordMap = new HashMap<>();
        stringWordMap.put("list", new Word("lit"));
        when(lineToWordsBreaker.getWords(songInfo)).thenReturn(stringWordMap);

        final SongAnalyzer songAnalyzer = new SongAnalyzer(singletonList(wordFunction), lineToWordsBreaker);
        List<SongStatisticEntity> songStatisticEntities = songAnalyzer.analyzeSong(singletonList(songInfo));

        SongStatisticEntity songStatisticEntity = songStatisticEntities.get(0);

        Assertions.assertNotNull(songStatisticEntity.getDate());
        Assertions.assertNotNull(songStatisticEntity.getTimestamp());
        Assertions.assertEquals(songStatisticEntity.getContent(), "line");
        Assertions.assertEquals(songStatisticEntity.getContentUrl(), "url");
        verify(lineToWordsBreaker).getWords(songInfo);
    }

    @Test
    public void statisticsEntityDataWillBeSetWhenWordFunctionListIsNotEmpty() {
        List<WordFunction> wordFunctionList = new ArrayList<>();
        wordFunctionList.add(wordFunction);
        final SongInfo songInfo = new SongInfo("name", singletonList("line"), "url");
        Map<String, Word> stringWordMap = new HashMap<>();
        stringWordMap.put("list", new Word("lit"));
        when(lineToWordsBreaker.getWords(songInfo)).thenReturn(stringWordMap);

        final SongAnalyzer songAnalyzer = new SongAnalyzer(wordFunctionList, lineToWordsBreaker);

        final List<SongStatisticEntity> entities = songAnalyzer.analyzeSong(singletonList(songInfo));

        verify(wordFunction).apply(eq(stringWordMap), any(SongStatisticEntity.class));
    }
}