package com.chubuk.services;

import com.chubuk.Word;
import com.chubuk.entities.SongStatisticEntity;
import com.chubuk.exception.TextAnalyzerServiceException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class AmountSameWordsTest {

    private final WordFunction wordFunction = new AmountSameWords();

    @Test
    public void totalAmountSameWordsWillBeCalculate(){
        Map<String, Word> wordsMap = new HashMap<>();
        wordsMap.put("str1", new Word("str1"));
        wordsMap.put("str2", new Word("str2"));
        wordsMap.put("str3", new Word("str1"));
        wordsMap.put("str4", new Word("str1"));
        wordsMap.put("str5", new Word("str2"));
        wordsMap.put("str6", new Word("str4"));

        SongStatisticEntity statisticEntity = new SongStatisticEntity();

        wordFunction.apply(wordsMap, statisticEntity);

        Assertions.assertEquals(statisticEntity.getAmountSameWords(), 0);
    }

    @Test
    public void zeroWillBeReturnedWhenWordsMapIsEmpty() {
        Map<String, Word> wordsMap = new HashMap<>();

        SongStatisticEntity statisticEntity = new SongStatisticEntity();

        wordFunction.apply(wordsMap, statisticEntity);

        Assertions.assertEquals(statisticEntity.getAmountSameWords(), 0);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void runtimeExceptionWillBeThrownWhenWordsMapIsNull() {
        SongStatisticEntity statisticEntity = new SongStatisticEntity();

        wordFunction.apply(null, statisticEntity);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void runtimeExceptionWillBeThrownWhenSongStatisticEntityIsNull() {
        wordFunction.apply(new HashMap<>(), null);
    }
}