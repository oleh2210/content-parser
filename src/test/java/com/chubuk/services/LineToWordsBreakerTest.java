package com.chubuk.services;

import com.chubuk.SongInfo;
import com.chubuk.Word;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonList;

public class LineToWordsBreakerTest {

    private SongInfo songInfo;
    private LineToWordsBreaker lineToWordsBreaker;

    @Test
    public void stringWordMapWillBeNullWhenStringInfoLinesIsEmpty() {
        LineToWordsBreaker lineToWordsBreaker = new LineToWordsBreaker();
        Map<String, Word> stringWordMap = new HashMap<>();
        Assertions.assertEquals(lineToWordsBreaker.getWords(songInfo), null);
    }

    @Test
    public void stringWordMapWillBeNullWhenIncorrectSplitCharacters() {
        Map<String, Word> stringWordMap = new HashMap<>();
        final String string = "A! Vasya:Mudak";
        final SongInfo songInfo = new SongInfo("name", singletonList(string), "url");
        Assertions.assertEquals(string.toLowerCase(), "a! vasya:mudak");
        Assertions.assertEquals(string.split("[!?:\\-.,\\s]+").length, 3);
        Assertions.assertEquals(lineToWordsBreaker.getWords(songInfo), null);
    }
}